from django.contrib.admin.filters import RelatedOnlyFieldListFilter
from django.db.models import query
from django.shortcuts import render
from .models import nabung

# Create your views here.
def home(request):
    nabung_sampah = nabung.objects.all()
    context = {
        'nabung_sampah' : nabung_sampah,
    }
    return render(request, 'home.html', context)
