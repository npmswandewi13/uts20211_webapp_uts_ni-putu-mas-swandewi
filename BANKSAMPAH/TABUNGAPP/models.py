from django.db import models

# Create your models here.
class nabung(models.Model):
    # Ini buat input tanggal otomatis sesuai dengan penginputan
    tanggal = models.DateTimeField(auto_now_add=True, null=False) 
    
    # Ini list user yang sudah ada sesuai data
    list_user = [
        ('User A','User A'),
        ('User B','User B'),
        ('User C','User C'),
        ('User D','User D'),
        ('User E','User E'),
    ]
    # Saat input memilih user mana
    user = models.CharField(
        max_length=10,
        choices=list_user,
    )

    # sampah yang ditabung
    nama_sampah = models.CharField(max_length=20)
    variasi_jenis_sampah = [
        ('Plastik','Plastik'),
        ('Botol','Botol'),
        ('Kardus','Kardus'),
        ('Kertas','Kertas'),
    ]
    jenis_sampah = models.CharField(
        max_length=10,
        choices=variasi_jenis_sampah,
    )

    # Setoran (kg/unit)
    jumlah_setoran = models.IntegerField()

    # Total poin yang didapatkan
    total_poin = models.IntegerField()

    # Status tabungan
    jenis_status = [
        ('Approved','Approved'),
        ('Waiting','Waiting'),
    ]
    status = models.CharField(
        max_length=10,
        choices=jenis_status,
    )

    def __str__(self):
        return self.nama_sampah