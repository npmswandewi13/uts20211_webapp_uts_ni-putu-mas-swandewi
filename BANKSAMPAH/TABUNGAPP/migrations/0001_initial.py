# Generated by Django 3.2.8 on 2021-10-26 04:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='nabung',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_sampah', models.CharField(max_length=20)),
                ('enis_sampah', models.CharField(choices=[('Plastik', 'Plastik'), ('Botol', 'Botol'), ('Kardus', 'Kardus'), ('Kertas', 'Kertas')], max_length=10)),
                ('poin', models.IntegerField()),
                ('jumlah_setoran', models.IntegerField()),
            ],
        ),
    ]
